﻿define('app', ['ko','config'], function (ko,config) {
    var app = function () {

        this.activePage = ko.observable();
        this.pages = ko.observableArray();


        this.setActivePage = function (page) {
            if (this.activePage() !== null && typeof this.activePage() !== 'undefined' ) {
                this.activePage().navigateAway();
            }
            
            if (this.pages.indexOf(page) < 0) {
                this.pages.push(page);
            }

            this.activePage(page);
        };

        
    };

    return new app();
});