﻿define("pagecontrols", ['pagecontrols/page.home','pagecontrols/page.aboutus'], function (home,aboutus) {
    return {
        home: home,
        aboutus: aboutus
    };
});