﻿define('pagecontrols/page.aboutus', ['ko', 'pagecontrols/page.base'], function (ko, base) {
    var aboutUsPage = function () {
        "use strict";
        var self = this;
        base.apply(this, arguments);
        this.title = ko.observable("ABOUT US");

        //testing the observables with list views
        this.ponies = ko.observableArray([{ name: "Star Dance", gender: "male" },
        { name: "Lightning Flash", gender: "male" },
        { name: "Chocolate River", gender: "female" }]);
    };

    return new aboutUsPage("#/aboutus", null, "aboutus");
});
