﻿//The page control takes care of navigation. Pages are kinda like controllers, give them a base route
define("pagecontrols/page.base", ['ko','config','app','lib/animation'], function (ko,config,app,animate) {
    var basePageControl = function (route,callback,pageName) {        
        "use strict";

        var navigate;
        var self = this;                        
        this.active = ko.observable(false);        
        this.pageName = ko.observable(pageName);
                
        //Add actions to the page via Action, maps
        //a sammy route and corresponding callback
        //only supports get actions at the moment
        this.Action = function (route, callback) {
            Sammy(config.appSelector,function(){
                this.get(self.baseUrl + "/" + route,callback);
            });
        }

        this.fadeOut = function (page) {
            $(page).fadeOut();
        }

        this.fadeIn = function (page) {
            $(page).fadeIn();            
        }

        //configure a per page url for getting templates
        //based on configurations in the config object       
        this.templateUrl = config.baseTemplateUrl;        

        //the main template direcory is by default pagename/index e.g. 
        this.templateName = ko.computed(function () {
            return self.pageName() + '/index';
        })

        this.destroy = function (remove) {
            //not a perfect way to clean up but better than nothing.
            //ko currently has no way of truly unapplying bindings.
            //https://github.com/SteveSanderson/knockout/issues/724
            ko.cleanNode($(this.selector));
            if (remove) {
                ko.removeNode($(this.selector));
            }
        };
        
        //called when arriving at this page
        this.navigate = function (callback) {                                    
            this.active(true);
            callback();
            app.setActivePage(self);
        };

        //call this method when leaving context of this page for another page
        this.navigateAway = function () {           

            //perhaps consider knockout postbox as a way for page events like this to be subscribed to.
            this.active(false);            
        };
              
        //set up routing.
        if (route !== null && typeof route !== 'undefined') {            
            this.baseUrl = route;

            //Sammy provides routing
            Sammy(config.appSelector,function() {
                //main route to this page
                this.get(route, function () {
                    self.navigate(callback || function () { });
                });
            });            
        }
        else {
            throw "Base route invalid";
        }
              
    };

    return basePageControl;
});