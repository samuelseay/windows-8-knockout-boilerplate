﻿define('config', [], function () {
    var config = {
        //The base JQuery selector for the app
        appSelector: "#app",
        //the base URL where all templates for pages are found
        baseTemplateUrl: "/pages",
        //the starting route for Sammy
        startPage: "#/home"        
    };

    return config;
});