﻿// For an introduction to the Navigation template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232506
(function () {
    "use strict";

    WinJS.Binding.optimizeBindingReferences = true;
    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;
    
    //define functions
    var boot;
    var define3rdPartyModules;
    var definePageControls;
    var loadPlugins;
    var runApp;
    
    define3rdPartyModules = function () {
        define('jquery', [], function () { return jQuery; });
        define('ko', [], function () { return ko; });
        define('sammy', [], function () { return Sammy; });
        define('infuser', [], function () { return infuser; });
    };

    require.config({
        //the base url of all scripts
        baseUrl: '/js',
        paths: {
            pagecontrols: 'pagecontrols',
            viewmodels: 'viewmodels',
            lib: 'lib'
        }
    });

    boot = function () {
        define3rdPartyModules();
        loadPlugins();            
    };

    loadPlugins = function () {
        // Plugins must be loaded after jQuery and Knockout, since they depend on them.
        requirejs([
            'lib/koExternalTemplateEngine-amd',
            'custombindings'
        ], runApp);
    };

    //define all page controls and run up the application (routes) as well as winJS application which has useful state info
    runApp = function () {
        //TODO Maybe wrap this up in the app I think.
        require(['pagecontrols', 'config', 'app'], function (pgControls, config, appViewModel) {
            var application = Sammy(config.appSelector, function () { this.get("#/", function () { }); });
            ko.applyBindings(appViewModel, $("#app")[0]);            
            application.run(config.startPage);
        });

        app.start();
    };
    
    app.addEventListener("activated", function (args) {
        if (args.detail.kind === activation.ActivationKind.launch) {
            if (args.detail.previousExecutionState !== activation.ApplicationExecutionState.terminated) {
                // TODO: This application has been newly launched. Initialize
                // your application here.
            } else {
                // TODO: This application has been reactivated from suspension.
                // Restore application state here.
            }

            if (app.sessionState.history) {
                nav.history = app.sessionState.history;
            }
            args.setPromise(WinJS.UI.processAll().then(function () {
                if (nav.location) {
                    nav.history.current.initialPlaceholder = true;
                    return nav.navigate(nav.location, nav.state);
                } else {
                    return nav.navigate(Application.navigator.home);
                }
            }));
        }
    });

    app.oncheckpoint = function (args) {
        // TODO: This application is about to be suspended. Save any state
        // that needs to persist across suspensions here. If you need to 
        // complete an asynchronous operation before your application is 
        // suspended, call args.setPromise().

        //TODO save history here.
        
    };

    boot();
})();
