﻿define('pagecontrols/page.home', ['ko','pagecontrols/page.base','viewmodels/vm.dashboard'], function (ko,base, dashboardViewModel) {
    var homePage = function () {
        "use strict";
        var self = this;
        //call parent
        base.apply(this, arguments);

        this.title = ko.observable("HOME");        
        this.dashboard = new dashboardViewModel();
    };

    return new homePage("#/home", null, "home");
});
