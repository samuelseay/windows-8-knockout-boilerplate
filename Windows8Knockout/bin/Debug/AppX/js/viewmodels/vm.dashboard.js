﻿define('viewmodels/vm.dashboard', ['viewmodels/vm.base', 'ko'], function (base, ko) {
    var dashboard = function () {
        //always inherit view models from base
        //to get navigation and other common functionality
        base.apply(this);

        //test things out
        this.testObservable = ko.observable("I'm a cool observable, why don't you use me?");
    };

    return dashboard;
});