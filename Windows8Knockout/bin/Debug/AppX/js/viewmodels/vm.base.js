﻿//The Base View Model that all view models should inherit from
define('viewmodels/vm.base', [], function () {

    //base view models could have a route of their own that they understand so that
    //throwing them into a page object means they just work without any extra code.
    //perhaps an idea could be to pass the base route of a page to any view model
    //you construct so that when it is constructed in the page it will have an immediate route defined.
    //or simply pass the viewmodel a route. navigating away from a view model is always about leaving
    //it, if you're going somewhere else its cause you want to do something else, so viewmodels are the smallest 
    //components of functionality in this app
    var base = function () {
        this.navigate = function (data, event) {
            var foo = "foo";
        }
    }

    return base;
});